This is a testing project

# test the application using cypress, run the below command

npm run cy:run

After running the above command, the report will generate in the cypress/reports folder and recording will generate in the cypress/videos folder 

# test cases 

Cypress test cases are written in the cypress/integration folder