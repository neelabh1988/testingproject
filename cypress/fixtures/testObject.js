export const testObj = {
    contactUs: '.elementor-element-1276730 > .elementor-widget-container > .elementor-heading-title',
    fullName: '#form-field-name',
    phoneNum: '#form-field-email',
    emailId: '#form-field-field_1',
    textField: '#form-field-message',
    sendBtn: 'div.elementor-field-group.elementor-column.elementor-field-type-submit.elementor-col-100.e-form__buttons > button',
    successMessage: 'div.elementor-message.elementor-message-success'
}